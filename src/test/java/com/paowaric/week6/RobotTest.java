package com.paowaric.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldleftOver() {
        Robot robot = new Robot("Robot", 'R',0 , Robot.MAX_Y);
        assertEquals(false, robot.left());
        assertEquals(Robot.MAX_Y, robot.getY());
    }

    @Test
    public void shouldCreateRobotSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }
    @Test
    public void shouldCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldUpNegative() {
        Robot robot = new Robot("Robot", 'R',0 , Robot.MIN_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.MIN_Y, robot.getY());
    }

    @Test
    public void shouldleftSuccess() {
        Robot robot = new Robot("Robot", 'R',2 , 0);
        assertEquals(true, robot.left());
        assertEquals(1, robot.getX());
    }

    @Test
    public void shouldUpSuccess() {
        Robot robot = new Robot("Robot", 'R',0 , 1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldLeftNegative() {
        Robot robot = new Robot("Robot", 'R',Robot.MIN_X , 0);
        assertEquals(false, robot.left());
        assertEquals(Robot.MIN_X, robot.getX());
    }

    @Test
    public void shouldLeftSuccess() {
        Robot robot = new Robot("Robot", 'R',1 , 0);
        assertEquals(true, robot.left());
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRightNegative() {
        Robot robot = new Robot("Robot", 'R',Robot.MAX_X , 0);
        assertEquals(false, robot.right());
        assertEquals(Robot.MAX_X, robot.getX());
    }

    @Test
    public void shouldRightSuccess() {
        Robot robot = new Robot("Robot", 'R',0 , 0);
        assertEquals(true, robot.right());
        assertEquals(1, robot.getX());
    }

    @Test
    public void shouldUpNSuccess() {
        Robot robot = new Robot("Robot", 'R',10 , 11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());
    }

    @Test
    public void shouldUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R',10 , 11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldUpNFail() {
        Robot robot = new Robot("Robot", 'R',10 , 11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldLeftNSSuccess() {
        Robot robot = new Robot("Robot", 'R',5 , 11);
        boolean result = robot.left(2);
        assertEquals(true, result);
        assertEquals(3, robot.getX());
    }

    @Test
    public void shouldLeftNSSuccess2() {
        Robot robot = new Robot("Robot", 'R',19 , 11);
        boolean result = robot.left(4);
        assertEquals(true, result);
        assertEquals(15, robot.getX());
    }

    @Test
    public void shouldLeftNFail() {
        Robot robot = new Robot("Robot", 'R',5 , 11);
        boolean result = robot.left(6);
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldDownNSuccess() {
        Robot robot = new Robot("Robot", 'R',0 , 6);
        boolean result = robot.down(4);
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldDownNSuccess2() {
        Robot robot = new Robot("Robot", 'R',0 , 6);
        boolean result = robot.down(5);
        assertEquals(true, result);
        assertEquals(11, robot.getY());
    }

    @Test
    public void shouldDownNFail() {
        Robot robot = new Robot("Robot", 'R',0 , 6);
        boolean result = robot.down(15);
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldRightNSuccess() {
        Robot robot = new Robot("Robot", 'R',12 , 6);
        boolean result = robot.right(2);
        assertEquals(true, result);
        assertEquals(14, robot.getX());
    }

    @Test
    public void shouldRightNSuccess2() {
        Robot robot = new Robot("Robot", 'R',12 , 6);
        boolean result = robot.right(7);
        assertEquals(true, result);
        assertEquals(19, robot.getX());
    }

    @Test
    public void shouldRightNFail() {
        Robot robot = new Robot("Robot", 'R',12 , 6);
        boolean result = robot.right(9);
        assertEquals(false, result);
        assertEquals(19, robot.getX());
    }
}
