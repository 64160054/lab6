package com.paowaric.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldWithdrawSuccess() {
        BookBank book = new BookBank("Paowaric", 100.0);
        book.withdraw(50);
        assertEquals(50, book.GetBalance(), 0.00001);
    }

    @Test
    public void shouldWithdrawOverBalance() {
        BookBank book = new BookBank("Paowaric", 100.0);
        book.withdraw(150);
        assertEquals(100, book.GetBalance(), 0.00001);
    }

    @Test
    public void shouldWithdrawWithNegativeNumber() {
        BookBank book = new BookBank("Paowaric", 100.0);
        book.withdraw(-100);
        assertEquals(100, book.GetBalance(), 0.00001);
    }

    @Test
    public void shouldDepositSuccess() {
        BookBank book = new BookBank("Paowaric", 100.0);
        book.deposit(50);
        assertEquals(150, book.GetBalance(), 0.00001);
    }

    @Test
    public void shouldDepositNegativeNumber() {
        BookBank book = new BookBank("Paowaric", 100.0);
        book.deposit(-50);
        assertEquals(100, book.GetBalance(), 0.00001);
    }
}
