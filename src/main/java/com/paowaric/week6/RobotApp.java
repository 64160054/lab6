package com.paowaric.week6;

public class RobotApp {
    public static void main(String[] args) {
        Robot klapong = new Robot("klapong", 'B', 1, 0);
        Robot plathong = new Robot("plathong", 'P', 10, 10);
        klapong.print();
        klapong.right();
        klapong.print();
        plathong.print();

        for (int y = Robot.MIN_Y; y < Robot.MAX_Y; y++) {
            for (int x = Robot.MIN_X; x < Robot.MAX_X; x++) {
                if(klapong.getX() ==  x && klapong.getY() == y) {
                    System.out.print(klapong.getSymbol());
                } else if(plathong.getX() ==  x && plathong.getY() == y) {
                    System.out.print(plathong.getSymbol());
                } else {
                    System.out.print("-");
                }
                
            }
            System.out.println();
        }
    }
}
