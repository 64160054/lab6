package com.paowaric.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank paowaric = new BookBank("Paowaric", 50.0);
        
        paowaric.print();
        BookBank fogus = new BookBank("Fogus", 1500.0);
        fogus.print();
        fogus.withdraw(500.0);
        fogus.print();

        paowaric.deposit(40000.0);
        paowaric.print();

        BookBank plub = new BookBank("Plub", 10000.0);
        plub.print();
        plub.deposit(5000.0);
        plub.print();
    }
}
