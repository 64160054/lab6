package com.paowaric.week6;

public class BookBank {
    // Attributes
    private String name;
    private double balance;
    public BookBank(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public boolean deposit(double money) {
        if (money < 1)
            return false;
        balance = balance + money;
        return true;
    }

    public boolean withdraw(double money) {
        if (money < 1)
            return false;
        if (money > balance)
            return false;
        balance = balance - money;
        return true;
    }

    public void print() {
        System.out.println(name + " " + balance);
    }

    public String GetName() {
        return name;
    }

    public double GetBalance() {
        return balance;
    }
}
